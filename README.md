# dns-failover

A tool used to automatically update CloudFlare DNS records when a host goes down.

This tool will also send out emails through SendGrid.

If you do not want to enable the email feature, leave out the `EMAILS` section in your env.js file.

There is currently no plans to support any other DNS service, or email provider. Sorry :confused:

The only configuration required is an `env.js` file located in src. You can place this file elsewhere and use the env var `DNS_FAILOVER_ENV` to specify the path.

A fully customized `env.js` can be found in `src/env.example.js`. The file is mostly commented, very obvious fields have not been commented.

## Building

```
# Install yarn
$ brew install yarn
# OR
$ npm install -g yarn

# Install deps, run server
$ yarn
$ yarn start
```

