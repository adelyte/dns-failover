import http from 'http'
import CF from 'cloudflare'
import nodemailer from 'nodemailer'
import sgTransport from 'nodemailer-sendgrid-transport'

const ENV = require(process.env.DNS_FAILOVER_ENV || './env')

const CF_AUTH = ENV.CF_AUTH
const client = new CF(CF_AUTH)

var SG_OPTIONS = { auth: ENV.SG_AUTH }
var mailer = nodemailer.createTransport(sgTransport(SG_OPTIONS))

class DomainWatcher {
  constructor ({ options, dns }) {
    this.dns = dns
    this.options = options
    this.downCount = 0

    this.onBackup = false

    this.monitorDomain()
  }

  monitorDomain () {
    this.monitorInterval = setInterval(() => {
      const callback = res => {
        const success = (() => {
          if (res.statusCode >= 200 && res.statusCode <= 299) {
            return true
          }

          return false
        })()

        if (success) {
          this.downCount = 0
        } else {
          this.downCount += 1
          console.log('Domain is down, count:', this.downCount)
        }

        if (!this.onBackup && this.downCount >= (this.options.threshold || 5)) {
          console.log('Falling back to backup record')
          this.updateRecord('backup')
          this.onBackup = true
        }

        if (this.downCount === 0 && this.onBackup) {
          console.log('Reverting to primary record')
          this.updateRecord('primary')
          this.onBackup = false
        }
      }

      try {
        http.get(this.options.endpoint, callback).on('error', () => {
          callback({ statusCode: -1 })
        })
      } catch (e) {
        callback({ statusCode: -1 })
      }
    }, this.options.poll)
  }

  updateRecord (newRecord) {
    for (const record of this.dns) {
      client.browseZones({
        name: record.domain
      }).then(result => {
        if (result.count === 0) {
          console.error("Couldn't find any records...")
          console.error(result)
        }

        if (result.count > 1) {
          console.error('Found too many records.')
          console.error(result)
          return
        }

        const domainId = result.result[0].id

        client.browseDNS(domainId, {
          type: record.type,
          name: record.name
        }).then(dnsResult => {
          const newDnsRecordConfig = {
            proxied: true,
            zoneId: domainId,
            type: record.type,
            name: record.name,
            content: record[newRecord]
          }

          if (dnsResult.count < 1) {
            const newDNSRecord = CF.DNSRecord.create(newDnsRecordConfig)

            client.addDNS(newDNSRecord).then(createResult => {
              this.sendMail(newRecord)
            })
          } else if (dnsResult.count === 1) {
            newDnsRecordConfig.id = dnsResult.result[0].id
            const updatedDNSRecord = CF.DNSRecord.create(newDnsRecordConfig)

            client.editDNS(updatedDNSRecord).then(updateResult => {
              this.sendMail(newRecord, record)
            })
          }
        }).catch(err => {
          console.error(err)
        })
      })
    }
  }

  sendMail (newRecord, record) {
    const PRIMARY_MESSAGE = {
      subject: '[FAILOVER-UP] Service has been restored',
      text: `Service to ${record.name} has been restored. The DNS record has been updated to ${record.primary}`
    }

    const BACKUP_MESSAGE = {
      subject: '[FAILOVER-DOWN] Service has failed',
      text: `Service to ${record.name} has failed. The DNS record has been updated to ${record.backup}`
    }

    const sendMailTo = (address) => {
      var email = {
        to: address,
        from: ENV.EMAILS.from,
        subject: (newRecord === 'backup' ? BACKUP_MESSAGE : PRIMARY_MESSAGE).subject,
        text: (newRecord === 'backup' ? BACKUP_MESSAGE : PRIMARY_MESSAGE).text
      }

      mailer.sendMail(email, (err, res) => {
        if (err) {
          console.log('sendMail error')
          console.log(err)
        }

        console.log('sendMail res', res)
      })
    }

    if (ENV.EMAILS.private) {
      for (const address of ENV.EMAILS.addresses) {
        sendMailTo([address])
      }
    } else {
      sendMailTo(ENV.EMAILS.addresses)
    }
  }
}

const instances = []
const main = () => {
  for (const domain of ENV.DOMAINS) {
    instances.push(new DomainWatcher(domain))
  }
}

main()
