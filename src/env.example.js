/* eslint-disable no-unused-vars */
const ATTEMPTS = 1
const MILLISECOND = 1
const MILLISECONDS = MILLISECOND
const SECOND = 1000 * MILLISECONDS
const SECONDS = SECOND
const MINUTE = 60 * SECONDS
const MINUTES = MINUTE
/* eslint-enable no-unused-vars */

module.exports = {
  CF_AUTH: {
    email: 'email@example.com',
    key: 'abcdefg'
  },
  SG_AUTH: {
    api_key: 'SG.abcdefg'
  },
  DOMAINS: [{
    options: {
      poll: 6 * SECONDS, // How often to check 'endpoint'.
      endpoint: 'http://example.com', // The address to attempt to connect to. Must be prefaced with http[s]://
      threshold: 3 * ATTEMPTS // Number of failed attempts to allow before failing over to backup dns records.
    },
    dns: [{ // The list of records to update for a particular record. Good when www and @ are the same CNAME/A record.
      domain: 'example.com', // The domain that you are scoped under
      type: 'CNAME', // CNAME/A/AAAA/MX/etc.
      name: 'example.com', // The name of the record you're wanting to update.
      primary: 'primary.example-host.com', // The primary DNS record.
      backup: 'backup.example-host.com' // The record to switch to after threshold is met.
    }, {
      domain: 'example.com',
      type: 'CNAME',
      name: 'www.example.com',
      primary: 'primary.example-host.com',
      backup: 'backup.example-host.com'
    }, {
      domain: 'example.com',
      type: 'A',
      name: 'a-record.example.com',
      primary: '192.168.0.1',
      backup: '192.168.0.2'
    }]
  }],
  EMAILS: {
    private: true, // Determines whether or not to send the emails out one by one or as a single email
    from: 'dns-failover@example.com',
    addresses: [ // List of emails, this can have SMS email addresses, good for a quick and dirty text.
      'ops@example.com',
      '5555555555@vtext.com'
    ]
  }
}
