const http = require('http')

http.createServer((req, res) => {
  res.statusCode = process.env.STATUS_CODE || 200

  console.log('.')

  res.end()
}).listen(8888)
